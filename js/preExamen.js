function cargarDatos(){
    const url = "https://jsonplaceholder.typicode.com/users"; 

    axios.get(url)
    .then((res)=>{
        mostrar(res.data);
    })
    .catch((err)=>{
        console.log("Surgió un error: " + err);
    })

    function mostrar(data){
        let id = document.getElementById("id").value;
        let nombre = document.getElementById("nombre");
        let nombreUser = document.getElementById("nombreUser");
        let email = document.getElementById("email");
        let calle = document.getElementById("calle");
        let numero = document.getElementById("numero");
        let ciudad = document.getElementById("ciudad");
        let bandera = false;

        for(item of data){
            if(id == item.id){
                nombre.value = item.name; 
                nombreUser.value = item.username;
                email.value = item.email;
                calle.value = item.address.street; 
                numero.value = item.address.suite;
                ciudad.value = item.address.city;
            }else{
                console.log("ocurrió un error");           
            }
            if(id == 0 || id == "" || id>10){
                if(!bandera){
                    alert("Elige un valor entre el 1 al 10");
                    bandera = true;
                }
            }
        }
    }
}

document.getElementById("btnBuscar").addEventListener('click', function(){
    cargarDatos();
});

document.getElementById("btnLimpiar").addEventListener('click', function limpiar(){
    id.value = "";
    nombre.value="";
    nombreUser.value="";
    email.value="";
    calle.value="";
    numero.value="";
    ciudad.value="";
})
